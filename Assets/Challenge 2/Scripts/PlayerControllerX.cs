﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerX : MonoBehaviour
{
    public GameObject dogPrefab;
    [SerializeField]
    private Transform DogSpawnPoint;



    private float timer = 1.0f;

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;
        // On spacebar press, send dog
        if (Input.GetKeyDown(KeyCode.Space) && timer <= 0.0f)
        {
            Instantiate(dogPrefab, DogSpawnPoint.position, dogPrefab.transform.rotation);
            timer = 1.0f;
        }
    }
}
