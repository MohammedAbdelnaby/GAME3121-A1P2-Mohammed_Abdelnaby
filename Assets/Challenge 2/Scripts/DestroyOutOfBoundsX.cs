﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyOutOfBoundsX : MonoBehaviour
{
    private float leftLimit = -30;
    private float bottomLimit = -5;
    private Text GameOver;
    private void Start()
    {
        GameOver = GameObject.FindGameObjectWithTag("TEXT").GetComponent<Text>();
    }
    // Update is called once per frame
    void Update()
    {
        // Destroy dogs if x position less than left limit
        if (transform.position.x < leftLimit)
        {
            Destroy(gameObject);
        } 
        // Destroy balls if y position is less than bottomLimit
        else if (transform.position.y < bottomLimit)
        {
            Time.timeScale = 0;
            Debug.Log("Game Over!");
            foreach (var item in FindObjectsOfType<MoveForwardX>())
            {
                Destroy(item.gameObject);
            }
            GameOver.enabled = true;
            Destroy(gameObject);
        }

    }
}
